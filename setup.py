from setuptools import setup, find_packages

setup(
	name="colorful-funktions",
	version="0.0.1",
	description="colorful functions",
	author="Gueney Yilmaz",
	author_email="gy004@hdm-stuttgart.de",
	packages=find_packages(),
	install_requires=["colorama"]
)
