from colorama import init, Fore, Back, Style
def print_colored(value: str, color="black") -> str:
	"""
	colors: red, black, white, blue
	"""
	colors_dict = {"red":Fore.RED, "black":Fore.BLACK, "white":Fore.WHITE, "blue":Fore.BLUE}
	print(colors_dict[str(color)] + value)
	
	
